// ==========================================================================
// Autores: Daniel Ramirez, Ramon Collazo
// Num Est : 801-12-6735, 801-12-1480
// emails  : bojangles7856@gmail.com, rlcmartis@gmail.com
// 
// Descripcion: Este es el archivo cliente del SuperString con la
// implementacion del evalpostfix.  Si es valido la expresion,
// despliega el resultado.
//
// ====================================================================

#include <iostream>
#include "superSt.h"
#include<cassert>

using namespace std;

void testEvalInfix() {
    int res;
    SuperString st;
    st = "( 9 + 5 ) / ( 10 - 3 )";

    assert(st.evalInfix(res));
    assert(res == 2);

    st = "( 9 + 5 * 1 ) * ( 1 - 2 )";
    assert(st.evalInfix(res));
    assert(res == -14);

    st = "( 9 + 5 * 1 ))";
    // this evalInfix should be false because of 
    // parenthesis unbalance
    assert(st.evalInfix(res)==false);  

}

int main() {
	testEvalInfix();
	return 0;
}
