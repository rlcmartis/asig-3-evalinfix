// ==========================================================================
// Autores: Daniel Ramirez, Ramon Collazo
// Num Est : 801-12-6735, 801-12-1480
// emails  : bojangles7856@gmail.com, rlcmartis@gmail.com
// 
// Descripcion: Este es un archivo que contiene la implementacion del
// evalposfix.
//      
//
// ===========================================================================
#include "superSt.h"
#include <vector>
#include <cstdlib>
#include <sstream>
#include <stack>

// Tokenizes (splits) the SuperString, based on the delimiter
// Parameters: delim: the delimiter
// Returns: a vector of SuperStrings that were split

vector<SuperString> SuperString::tokenize(const string & delim) {
	vector<SuperString> tokens;
	size_t p0 = 0, p1 = string::npos;
	while(p0 != string::npos) {
    	p1 = find_first_of(delim, p0);
    	if(p1 != p0) {
      		string token = substr(p0, p1 - p0);
      		tokens.push_back(token);
      	}
		p0 = find_first_not_of(delim, p1);
	}
	return tokens;
}

// Overload of the operator= for using stataments such as:
// SuperString st = static_cast<string>("3 4 5 + * 1 +");
SuperString& SuperString::operator= (string s) {
	this->assign(s);
	return *this;
}


// Overload of the operator= for using stataments such as:
// SuperString st = 209;
SuperString& SuperString::operator= (int n) {
	this->assign(dynamic_cast< std::ostringstream & >(( std::ostringstream() 
   		<< std::dec << n ) ).str());
	return *this;
}

// Overload of the constructor, allows statements such as:
// SuperString st(254);
SuperString::SuperString(int a) {
   this->assign(dynamic_cast< std::ostringstream & >(( std::ostringstream() 
   		<< std::dec << a ) ).str());
}

// Converts the SuperString to int, if possible
bool SuperString::toInt(int& i) {
   char c = this->c_str()[0];
   if(empty() || ((!isdigit(c) && (c != '-') && (c != '+')))) return false ;

   char * p ;
   i = strtol(c_str(), &p, 10) ;

   return (*p == 0) ;
}
	

// Evaluates the postfix expression.
// Returns:
//   - result: integer through which we return the result of the eval
//   - boolean return value: true if the expression is valid
bool SuperString::evalPostfix(int &result) {
	//Creates vector and separates them
   	vector<SuperString> V = tokenize(" ");
	//Creates stack for ints 
	stack<int> S;
	int n;
 
	for (int i=0; i < V.size(); i++){
		int a,b; //Used to evaluate the postfix 
		if (V[i].toInt(n))	
			S.push(n);

		else {
			if (S.empty()) return false;
			if (V[i] == "+"){
				//Sets the top to a
				a=S.top();
				S.pop(); //Removes the top element
				
				//If it becomes empty, stops the func
				if (S.empty()) return false;
				//Sets the top to b
				b=S.top(); 
				S.pop();   //Removes the top element
				S.push(b+a); //Pushes the sum of b and a
			}
			if (V[i] == "-"){
                                //Sets the top to a
                                a=S.top();
                                S.pop(); //Removes the top element

				//If it becomes empty, stops the func
                                if (S.empty()) return false;

                                //Sets the top to b
                                b=S.top();
                                S.pop();   //Removes the top element
                                S.push(b-a); //Pushes the sub of b and a
                        }
			if (V[i] == "*"){
                                //Sets the top to a
                                a=S.top();
                                S.pop(); //Removes the top element

				//If it becomes empty, stops the func
                                if (S.empty()) return false;

                                //Sets the top to b
                                b=S.top();
                                S.pop();   //Removes the top element
                                S.push(b*a); //Pushes the mult of b and a
                        }
			if (V[i] == "/"){
                                //Sets the top to a
                                a=S.top();
                                S.pop(); //Removes the top element

				//If it becomes empty, stops the func
                                if (S.empty()) return false;

                                //Sets the top to b
                                b=S.top();
                                S.pop();   //Removes the top element
                                S.push(b/a); //Pushes the div of b and a
                        }

		}
		
	}
	//If the stack contains only one element, returns it
	if (S.size()== 1){
		//Returns the value of the top element in the array
		result = S.top();
		return true;
	}
	//Else, returns false
	else 
		return false;
}

// Evaluates the infix expression.
// Returns:
//   - result: integer through which we return the result of the eval
//   - boolean return value: true if the expression is valid
bool SuperString::evalInfix(int & num){

   stack<string> Op; //A stack that will contain the operators
   stack<int> No;    //A stack that will contain the numbers
   
   //We create a vector that will contain on each position an operator
   //or operand of the infix expression
   vector<SuperString> V = tokenize(" ");
   
   //We go througouht the vector and doing different operations
   //depending of what the element is
   for (int i = 0; i < V.size(); i++){
      //If it's a left side parenthesis just insert it in the Op stack
      if (V[i] == "(")
         Op.push("(");
      //If it's a minor priority operator we have to check something..
      else if (V[i] == "+" || V[i] == "-"){
         //While on the top of the Op there is a major priority operator
         //we have to solve it before inserting the minor priority operator
         while(Op.top() == "*" || Op.top() == "/"){
            num = No.top();
            No.pop();
            if (Op.top() == "*")
               num = num * No.top();
            else
               num = No.top() / num;
            No.pop();
            No.push(num);
            Op.pop();
         }
         Op.push(V[i]);
      }
      //If it's a major priority operand just put it in the Op stack
      else if (V[i] == "*" || V[i] == "/"){
         Op.push(V[i]);
      }
      //If it's a right hand parentesis we have to use the operators
      //until we find the pair of the parentesis
      else if (V[i] == ")"){
         //While the top of the Op it's not the other parentesis we solve
         //the operators
         while(Op.top() != "("){
            num = No.top();
            No.pop();
            if (Op.top() == "+")
                num = num + No.top();
            else if (Op.top() == "-")
                num = No.top() - num;
            else if (Op.top() == "*")
                num = num * No.top();
            else
                num = No.top() / num;
            No.pop();
            No.push(num);
            Op.pop();
         }
         Op.pop(); //Delete the "("
      }
      //If it is not any of the operators is because it is a number
      //so we insert it in the stack of numbers
      else{
	 V[i].toInt(num);
         No.push(num);
      }
   }

   //If there are some operators that have not being used we have to do it
   while(!Op.empty()){
        num = No.top();
        No.pop();
        if (Op.top() == "+"){
            num = num + No.top();
            No.pop();
        }
        else if (Op.top() == "-"){
            num = No.top() - num;
            No.pop();
        }
        else if (Op.top() == "*"){
            num = num * No.top();
            No.pop();
        }
        else if (Op.top() == "/"){
            num = No.top() / num;
            No.pop();
        }
        else;
        No.push(num);
        Op.pop();
   }

   //Now, it's supossed that, if the evaluation was successful we just
   //take the ONLY number in the stack of numbers and send it
   num = No.top();
   No.pop();

   //If when we delete the last element of the stack it is not empty is
   //because the evaluation was unsuccessful
   if (!No.empty())
      return false;
   return true;
}
